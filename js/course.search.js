var course_id = [];

var CourseSearch = {};

CourseSearch.model = {
add: function(id) {
    if (course_id.indexOf(id) > -1)
    {
        return false;
    }
    course_id.push(id);
    return true;
},

remove: function(id) {
    var index = course_id.indexOf(id);
    if (index > -1)
    {
        course_id.splice(index, 1);
    }
}
};

CourseSearch.action = {
add: function (id, text) {
    var span = new Element("span", {"class": "course", "data-course-id": id}).update(text);
    $("course_spans").insert(span);
    span.observe("click", CourseSearch.action.remove);
},

refresh: function() {
    course_id = course_id.uniq().sort();

    $("course_id").value = course_id.join(",");
    if (course_id.length === 0) {
        $$("#searchform label")[0].setAttribute("disabled", "disabled");
    } else {
        $$("#searchform label")[0].removeAttribute("disabled");
    }

},

remove: function (event) {
    CourseSearch.model.remove(this.getAttribute("data-course-id"));
    this.remove();
    CourseSearch.action.refresh();
},

select: function (li) {
    if (CourseSearch.model.add(li.id))
    {
        CourseSearch.action.add(li.id, li.textContent);
        CourseSearch.action.refresh();
    }

    $("autocomplete").value = "";
}
}
