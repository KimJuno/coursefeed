<?php

require_once("coursefeed.php");
require_once("http.php");

$coursefeed = new CourseFeed();

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (http\has_parameter($_GET, "autocomplete_parameter")) {
    	$result = $coursefeed->getCourseId($_GET["autocomplete_parameter"]);
        header('HTTP/1.1 200 OK');
    } else {
        header('HTTP/1.1 400 Bad Request');
        exit;
    }
} else {
    header('HTTP/1.1 405 Method Not Allowed');
    exit;
}

?>
<ul>
<? foreach($result as $row) { ?>
    <li id="<?= $row["id"] ?>"><?= $row["name"] ?> <?= $row["year"] ?></li>
<? } ?>
</ul>
