<? namespace url;

function query_string($dict, $diff=array()) {
    $qs = trim(http_build_query(array_merge($dict, $diff)));
    if (strlen($qs) > 0) {
        return "?" . $qs;
    }
    return "";
}

function getBaseURL() {
    return $_SERVER["HTTP_HOST"] . "/coursefeed/";
}

function getMailConfirmURL($user_id, $key) {
    return getBaseURL() . "certificate.php" .query_string(array("key"=> $key));
}

function getArticleURL($id) {
    return getBaseURL() . "article.php";
}

?>
