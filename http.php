<?php

namespace http;

function has_parameter($dict, $param) {
    if (array_key_exists($param, $dict)) {
        $value = $dict[$param];
        return isset($value) && $value !== "" && $value !== array();
    } else {
        return false;
    }
}

function maybe_get_parameter($dict, $param) {
    if (has_parameter($dict, $param)) {
        return $dict[$param];
    } else {
        return null;
    }
}

function query_string($dict, $diff=array()) {
    $qs = trim(http_build_query(array_merge($dict, $diff)));
    if (strlen($qs) > 0) {
        return "?" . $qs;
    }
    return "";
}

?>
