<?php

require_once("coursefeed.php");
require_once("template/certificate.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    startblock('content');
    if (http\has_parameter($_GET, "key")) {
        $key = $_GET['key'];
        $certificated_users = $coursefeed->certify($key);
        if (count($certificated_users) > 0) {
            template\certificate\renderCertificateForm();
        }
    }
    endblock('content');
}

?>
