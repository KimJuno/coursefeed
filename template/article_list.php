<? namespace template\article; ?>

<? function renderListHeader() { ?>
    <h2>Articles <a href="rss.php<?= \http\query_string($_GET, array("month"=>null))?>"><i class = "fa fa-rss-square"></i></a></h2>
<? } ?>

<? function renderList($articles, $page_num, $page_count, $course_id) { ?>
    <p>
    <? if (isset($_SESSION['user'])) { ?>
        <? if ($_SESSION['user']['level'] < 2) { ?>
            Confirm your certificate mail to write article.
        <? } ?>
        <!-- add by 이정훈 ADMIN delete 버튼 활성 -->
    <? } else { ?>
        Want to write a article? Login or <a href="join.php">join</a>.
    <? } ?>
    </p>
    <p>View as <a href="<?= \http\query_string($_GET, array('month'=>date('m')))?>">Calendar</a></p>
    <section id="articles">
    <? if (count($articles) > 0) { ?>
        <form action="article_delete.php" method="POST" class="pure-form" id="article-list">
        <? foreach ($articles as $article) { ?>
        <div class="article-entry">
        <? if (isset($_SESSION['user']) && $_SESSION['user']['level'] >= 99) { ?>
            <span><input type="checkbox" name="articles[]" value="<?=$article['id']?>"></span>
        <? } ?>
            <a href="article.php?id=<?=$article["id"]?>"><span class="headline"><?=htmlspecialchars($article["title"]) ?></span><span class="content"><?=htmlspecialchars(mb_substr($article["content"], 0, 30, "utf8")) ?></span>
            <span class="user"><?=$article["user_name"]?></span>
            </a>
            <span class="course"><?=$article["course_name"]?> <span class="category"><?=$article["category_name"]?></span></span>
            <span class="timestamp"><?=$article["uploaded_at"]?></span>
        </div>
        <? } ?>
        <? if (isset($_SESSION['user']) && $_SESSION['user']['level'] >= 2) { ?>
            <a class="pure-button pure-button-primary" href="article_edit.php">write</a>
        <? } ?>
        <? if (isset($_SESSION['user']) && $_SESSION['user']['level'] >= 99) { ?>
        <input type="submit" id="doDelete" value="delete" class="pure-button pure-button-warning" >
        <? } ?>
        </form>
    <? } else { ?>
        <p>no entry</p>
        <p><? if (isset($_SESSION['user']) && $_SESSION['user']['level'] >= 2) { ?>
            <a class="pure-button pure-button-primary" href="article_edit.php">write</a>
        <? } ?></p>
    <? } ?>
    </section>

    <!-- add by 이정훈, paginate bar !-->
    <ul class="paginator">
        <? if($page_count != 0) { ?>
            <? for ($i = max(($page_num-2) % $page_count, 1); $i <= $page_count; $i++) { ?>
                <li>
                    <? if ($i !== $page_num) { ?>
                    <a href="article.php<?= \http\query_string($_GET, array("page" => $i)) ?>">
                    <? } else { ?>
                    <span class="current">
                    <? } ?>
                        <?=$i?>
                    <? if ($i !== $page_num) { ?>
                    </a>
                    <? } else { ?>
                    </span>
                    <? } ?>
                </li>
            <? } ?>
        <? } ?>
    </ul>
    <script type="text/javascript">
    <? if ($course_id !== null) { ?>
        <? foreach ($course_id as $course) { ?>
            CourseSearch.action.select({id : '<?= $course["id"] ?>', textContent: '<?= sprintf("%s %s", $course["name"], $course["year"]) ?>'});
        <? } ?>
    <? } ?>
    </script>
<? } ?>
